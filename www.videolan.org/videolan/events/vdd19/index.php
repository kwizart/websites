<?php
   $title = "Video Dev Days 2019, November 9-10, 2019";
   $body_color = "orange";

   $new_design = true;
   $show_vdd_banner = false;
   $lang = "en";
   $menu = array( "videolan", "events" );

   $additional_js = array("/js/slimbox2.js", "/js/slick-init.js", "/js/slick.min.js");
   $additional_css = array("/js/css/slimbox2.css", "/style/slick.min.css", "/videolan/events/vdd19/style.css");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>
<script>
  countdownManager = {
    targetTime: new Date(),
    element: {
        day: null,
        hour: null,
        min: null,
        sec: null
    },
    interval: null,
    init: function(targetTime) {
        this.targetTime = targetTime || new Date();
        this.element.day  = $('#countdown-day');
        this.element.hour = $('#countdown-hour');
        this.element.min  = $('#countdown-min');
        this.element.sec  = $('#countdown-sec');

        this.tick();
        this.interval = setInterval('countdownManager.tick();', 1000);
    },

    tick: function() {
        var timeNow = new Date();
        if (timeNow > this.targetTime) {
            timeNow = this.targetTime;
            $('#countdown').hide();
            clearInterval(this.interval);
            return;
        }

        var diff = this.dateDiff(timeNow, this.targetTime);

        this.element.day.text(diff.day);
        this.element.hour.text(diff.hour);
        this.element.min.text(diff.min);
        this.element.sec.text(diff.sec);
    },

    dateDiff: function(date1, date2) {
        var diff = {};
        var tmp = date2 - date1;

        tmp = Math.floor(tmp / 1000);
        diff.sec = tmp % 60;
        tmp = Math.floor((tmp-diff.sec) / 60);
        diff.min = tmp % 60;
        tmp = Math.floor((tmp-diff.min) / 60);
        diff.hour = tmp % 24;
        tmp = Math.floor((tmp-diff.hour) / 24);
        diff.day = tmp;
        return diff;
    }
};

$(function($){
    countdownManager.init(new Date('2019-11-09'));
});

</script>
  <div class="sponsor-box-2">
    <h4>Sponsors</h4>
    <a href="https://www.iij.ad.jp/en/" target="_blank">
        <?php image( 'events/vdd19/sponsors/IIJ.jpg' , 'Internet Initiative Japan', 'sponsors-logo'); ?>
    </a>
  </div>
<header class="header-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 text-center">
        <img src="//images.videolan.org/images/VLC-IconSmall.png">
        <h1>Video Dev Days 2019</h1>
        <h3>The Open Multimedia Conference that frees the cone in you!</h3>
        <h4>9 - 10 November, 2019, Tokyo</h4>
<!--        <a href="https://goo.gl/forms/8WGREy2hMxRNfGzv2" class="btn btn-border vbtn-link">Register<span class="arrow right vdd-icon"></span></a>-->
        <p>Registration opens soon.</p>
      </div>
    </div>
    <div class="row">
      <div id="countdown">
        <div class="countdown-box">
          <span id="countdown-day" >--</span>
          <div class="countdown-unit">Days</div>
        </div>
        <div class="countdown-box">
          <span id="countdown-hour">--</span>
          <div class="countdown-unit">Hours</div>
        </div>
        <div class="countdown-box">
          <span id="countdown-min" >--</span>
          <div class="countdown-unit">Minutes</div>
        </div>
        <div class="countdown-box">
          <span id="countdown-sec" >--</span>
          <div class="countdown-unit">Seconds</div>
        </div>
      </div>
    </div>
  </div>
  <!-- <div class="container">
    <div id="sponsors">
        <h5>Sponsors</h5>
        <p>We are looking for sponsors</p>
    </div>
  </div> -->
</header>
<section id="overview">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 text-center">
        <h2 class="uppercase">
          Video Dev Days 2019
          <span class="spacer-inline">About</span>
        </h2>
        <p>The <a href="/videolan/">VideoLAN non-profit organisation</a> is happy to
        invite you to the multimedia open-source event of the summer!</p>
        <p>For its <b>eleventh edition</b>, people from the VideoLAN and open source multimedia communities will meet in <strong>Tokyo</strong>
        to discuss and work on the future of the open-source multimedia community</p>

        <p>This is a <strong>technical</strong> conference.</p>
        <div class="row">
          <div class="col-md-6">
            <div class="text-box when-box">
              <h4 class="text-box-title">When?</h4>
              <p class="text-box-content">9 - 10 November 2019</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box where-box">
              <h4 class="text-box-title">Where?</h4>
              <p class="text-box-content">Tokyo</p>
            </div>
          </div>
        </div>



      </div>
    </div>
  </div>
</section>
<!-- <section id="where" class="bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 text-center">
        <h2 class="uppercase">Where?</h2>
        <p>The venue is in Paris!</p>
      </div>
    </div>
  </div>
</section> -->

<section id="schedule">
  <div class="container">
    <div class="row">
      <div class="text-center">
        <h2 class="uppercase">Schedule</h2>
        <hr class="spacer-2">
      </div>
      <div class="col-lg-10 col-lg-offset-1">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="#friday" aria-controls="friday" role="tab" data-toggle="tab">Friday 8</a></li>
        <li role="presentation" class="active"><a href="#saturday" aria-controls="saturday" role="tab" data-toggle="tab">Saturday 9</a></li>
        <li role="presentation"><a href="#sunday" aria-controls="sunday" role="tab" data-toggle="tab">Sunday 10</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade" id="friday">
            <div class="event">
              <h4 class="event-time">09:30 - 18:30</h4>
              <div class="event-description">
                <h3>Community Bonding Day: Surprise!</h3>
                <p>This year we'll do a <b>??????????</b>!<br/>
                The VideoLAN organization will pay for whatever costs associated with the event.<br/></p>
              </div>
            </div>
            <div class="event">
              <h4 class="event-time">19:30</h4>
              <div class="event-description">
                <h3>Evening drinks</h3>
                <p>On <strong>Friday at 19h30</strong>, people are welcome to come and
                share a few good drinks, with all attendees. <!--, at the <a href="https://goo.gl/maps/tTTAPwjzHe62" target="_blank">King George pub</a>. --></p>
                <p>Alcoholic and non-alcoholic drinks will be available!</b>
              </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade active in" id="saturday">
            <div class="event event-breakfast">
              <h4 class="event-time">
                08:30 - 09:00
              </h4>
              <div class="event-description">
                <h3>Registration & Breakfast</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:00 - 12:30
              </h4>
              <p class="event-author">
                <span class="avatar avatar-jb"></span>Jean-Baptiste Kempf, VideoLAN
              </p>
              <div class="event-description">
                <h3>Talks</h3>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                12:30 - 14:00
              </h4>
              <div class="event-description">
                <h3>Lunch Break</h3>
              </div>
            </div>

            <div class="event event-meetups">
              <h4 class="event-time">
                14:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Meetups</h3>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                19:30 - ??:??
              </h4>
              <div class="event-description">
                <h3>Community Dinner</h3>
                <p> Location: Tokyo</p>
              </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="sunday">
            <div class="event event-breakfast">
              <h4 class="event-time">
                09:00 - 09:30
              </h4>
              <div class="event-description">
                <h3>Breakfast</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:30 - 12:00
              </h4>
              <div class="event-description">
                <h3>Lightning talks</h3>
                <ul>
                </ul>
              </div>
            </div>

            <div class="event event-bg event-lunch">
              <h4 class="event-time">
                12:00 - 14:00
              </h4>
              <div class="event-description">
                <h3>Lunch</h3>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                14:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Unconferences</h3>
              </div>
            </div>

            <div class="event">
              <div class="event-inner">
                <div class="event-description">
                    <p>The actual content of the unconference track is being decided on Saturday evening. For the live schedule, check the <a href="https://wiki.videolan.org/VDD19#Unconference_schedule">designated page on the wiki</a>.</p>
                </div>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                20:00 - ??:??
              </h4>
              <div class="event-description">
                <h3>Unofficial Dinner</h3>
                <p>Karaoke + Fishing?</p>
              </div>
            </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>

<section id="who-come">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-box" id="who-come-box">
        <h2 class="uppercase">Who can come?
        </h2>
        <p><strong>Anyone</strong> who cares about open source multimedia technologies and development. Remember that it targets a technical crowd!</p>
        <p>If you are representing a <b>company</b> caring about open-source multimedia software, we would be <b>very interested</b> if you could co-sponsor the event.</p>
      </div>

    </div>
  </div>
</section>

<section id="register-section">
  <div class="container">
      <h2 class="uppercase">Register</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="text-box register-box">
            <h3 class="text-box-title">
              COST AND SPONSORSHIP
            </h3>
            <h3>FREE</h3>
            <p class="text-box-content">
              The cost for attendance is free.

              Like previous years, active developers can get a full sponsorship covering travel costs. We will also provide accomodation.
            </p>
            <div class="ticket">
              Registration opens soon. <!-- <a href="https://goo.gl/forms/8WGREy2hMxRNfGzv2">HERE!</a> -->
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="text-box register-box">
            <h3 class="text-box-title">Accommodation</h3>
<!--            <p>For active members of the open-source multimedia communities who registered until August 25 and got a confirmation, your hotel will be one of the following:</p>
            <ul>
              <li>Hotel Monsieur, 62 rue des Mathurins, Paris 8
              <li>Mercure Paris Opéra Faubourg Montmartre, 5 Rue De Montyon, Paris 9
            </ul>
          </b></p> -->
                <p>Stay tuned</p>
          </div>
        </div>
      </div>
  </div>
</section>
<section>
  <div class="container">
    <h2 class="text-center uppercase">Sponsors</h2>
    <hr class="spacer-2">

    <p class="big-p text-center">
      We are looking for sponsors!
    </p>
    <section class="text-center">
      <a href="https://www.iij.ad.jp/en/" target="_blank">
        <?php image( 'events/vdd19/sponsors/IIJ.jpg' , 'Internet Initiative Japan', 'sponsors-logo-2'); ?>
      </a>
    </section>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Locations</h2>
    <hr class="spacer-2">
    <div class="row">
      <div class="col-md-6">
        <div class="text-box conference-box">
          <div class="text-box-title">
            Conference
          </div>
          <div class="text-box-content">
            Internet Initiative Japan Inc.
Iidabashi Grand Bloom, 2-10-2 Fujimi, Chiyoda-ku, Tokyo 102-0071, JAPAN
          </div>
        </div>
      </div>
      <!--<div class="col-md-6">
        <div class="text-box dinner-box">
          <div class="text-box-title">
              Saturday Dinner
          </div>
          <div class="text-box-content">
          </div>
        </div>
      </div> -->
    </div>

  </div>
</section>


<section>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="uppercase">Code of Conduct </h2>
        <p>This community activity is running under the <a href="https://wiki.videolan.org/CoC/">VideoLAN Code of Conduct</a>. We expect all attendees to respect our <a href="https://wiki.videolan.org/VideoLAN_Values/">Shared Values</a>.</p>
      </div>
      <div class="col-md-6">
        <h2 class="uppercase">Contact </h2>
        <p>The VideoLAN Dev Days are organized by the board members of the VideoLAN non-profit organization, Jean-Baptiste Kempf, Denis Charmet, Konstantin Pavlov and Hugo Beauz&eacute;e-Luyssen. You can reach us at <span style="color: #39b549">board@videolan.org</span>.</p>
      </div>
    </div>
  </div>
</section>



<?php footer('$Id: index.php 5400 2009-07-19 15:37:21Z jb $'); ?>
