# Dutch translation
# Copyright (C) 2016 VideoLAN
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Thomas De Rocker, 2012-2016
msgid ""
msgstr ""
"Project-Id-Version: VLC - Trans\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-16 04:20-0500\n"
"PO-Revision-Date: 2016-02-18 08:21+0000\n"
"Last-Translator: Thomas De Rocker\n"
"Language-Team: Dutch (http://www.transifex.com/yaron/vlc-trans/language/nl/)\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:291
msgid "a project and a"
msgstr "een project en een"

#: include/header.php:291
msgid "non-profit organization"
msgstr "non-profit organisatie"

#: include/menus.php:27
msgid "Team &amp; Organization"
msgstr "Team &amp; organisatie"

#: include/menus.php:28
msgid "Consulting Services &amp; Partners"
msgstr "Consulting services &amp; partners"

#: include/menus.php:29 include/footer.php:69
msgid "Events"
msgstr "Gebeurtenissen"

#: include/menus.php:30 include/footer.php:64 include/footer.php:98
msgid "Legal"
msgstr "Rechten"

#: include/menus.php:31 include/footer.php:68
msgid "Press center"
msgstr "Perscentrum"

#: include/menus.php:32 include/footer.php:65
msgid "Contact us"
msgstr "Contacteer ons"

#: include/menus.php:38 include/os-specific.php:260
msgid "Download"
msgstr "Download"

#: include/menus.php:39 include/footer.php:24
msgid "Features"
msgstr "Functies"

#: include/menus.php:40 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Aanpassen"

#: include/menus.php:42 include/footer.php:56
msgid "Get Goodies"
msgstr "Goodies verkrijgen"

#: include/menus.php:46
msgid "Projects"
msgstr "Projecten"

#: include/menus.php:64 include/footer.php:30
msgid "All Projects"
msgstr "Alle projecten"

#: include/menus.php:68 index.php:164
msgid "Contribute"
msgstr "Bijdragen"

#: include/menus.php:70
msgid "Getting started"
msgstr "Beginnen"

#: include/menus.php:71 include/menus.php:89
msgid "Donate"
msgstr "Doneren"

#: include/menus.php:72
msgid "Report a bug"
msgstr "Een bug rapporteren"

#: include/menus.php:76
msgid "Support"
msgstr "Ondersteuning"

#: include/footer.php:22
msgid "Skins"
msgstr "Skins"

#: include/footer.php:23
msgid "Extensions"
msgstr "Extensies"

#: include/footer.php:25 vlc/index.php:83
msgid "Screenshots"
msgstr "Screenshots"

#: include/footer.php:48
msgid "Community"
msgstr "Gemeenschap"

#: include/footer.php:51
msgid "Forums"
msgstr "Forums"

#: include/footer.php:52
msgid "Mailing-Lists"
msgstr "Maillijsten"

#: include/footer.php:53
msgid "FAQ"
msgstr "FAQ"

#: include/footer.php:54
msgid "Donate money"
msgstr "Geld doneren"

#: include/footer.php:55
msgid "Donate time"
msgstr "Tijd doneren"

#: include/footer.php:62
msgid "Project and Organization"
msgstr "Project en organisatie"

#: include/footer.php:63
msgid "Team"
msgstr "Team"

#: include/footer.php:66
msgid "Partners"
msgstr "Partners"

#: include/footer.php:67
msgid "Mirrors"
msgstr "Mirrors"

#: include/footer.php:70
msgid "Security center"
msgstr "Veiligheidscentrum"

#: include/footer.php:71
msgid "Get Involved"
msgstr "Raak betrokken"

#: include/footer.php:72
msgid "News"
msgstr "Nieuws"

#: include/os-specific.php:91
msgid "Download VLC"
msgstr "VLC downloaden"

#: include/os-specific.php:97 include/os-specific.php:276 vlc/index.php:168
msgid "Other Systems"
msgstr "Andere systemen"

#: include/os-specific.php:241
msgid "downloads so far"
msgstr "downloads tot nu toe"

#: include/os-specific.php:629
msgid "VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols."
msgstr "VLC is een gratis en open source cross-platform multimediaspeler en -framework dat de meeste multimediabestanden en ook dvd's, audio-cd's, vcd's en verschilllende streaming-protocols afspeelt."

#: include/os-specific.php:633
msgid "VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files, and various streaming protocols."
msgstr "VLC is een gratis en open source cross-platform multimediaspeler en framework. Het speelt de meeste multimedia-bestanden en verschillende streaming-protocollen af."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC: Officiële site - Gratis multimedia-oplossingen voor alle besturingssystemen!"

#: index.php:25
msgid "Other projects from VideoLAN"
msgstr "Andere projecten van VideoLAN"

#: index.php:29
msgid "For Everyone"
msgstr "Voor iedereen"

#: index.php:39
msgid "VLC is a powerful media player playing most of the media codecs and video formats out there."
msgstr "VLC is een krachtige mediaspeler die de meeste bestaande mediacodecs en videoformaten afspeelt."

#: index.php:52
msgid "VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr "VideoLAN Movie Creator is niet-lineaire bewerkingssoftware voor videocreatie."

#: index.php:61
msgid "For Professionals"
msgstr "Voor professionals"

#: index.php:71
msgid "DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr "DVBlast is een simpele en krachtige MPEG-2/TS demux- en streaming-toepassing"

#: index.php:81
msgid "multicat is a set of tools designed to easily and efficiently manipulate multicast streams and TS."
msgstr "multicat is een verzameling gereedschappen die ontwikkeld zijn om gemakkelijk en efficiënt multicast-streams en TS te manipuleren."

#: index.php:94
msgid "x264 is a free application for encoding video streams into the H.264/MPEG-4 AVC format."
msgstr "x264 is een gratis toepassing om videostreams te coderen in het H.264/MPEG-4 AVC-formaat."

#: index.php:103
msgid "For Developers"
msgstr "Voor ontwikkelaars"

#: index.php:136
msgid "View All Projects"
msgstr "Alle projecten weergeven"

#: index.php:140
msgid "Help us out!"
msgstr "Help ons!"

#: index.php:144
msgid "donate"
msgstr "doneren"

#: index.php:152
msgid "VideoLAN is a non-profit organization."
msgstr " VideoLAN is een non-profit organisatie."

#: index.php:153
msgid " All our costs are met by donations we receive from our users. If you enjoy using a VideoLAN product, please donate to support us."
msgstr "Al onze kosten worden gedekt door donaties die we ontvangen van onze gebruikers. Als u plezier heeft in het gebruik van een VideoLAN-product, kunt u doneren om ons te ondersteunen."

#: index.php:156 index.php:176 index.php:194
msgid "Learn More"
msgstr "Meer informatie"

#: index.php:172
msgid "VideoLAN is open-source software."
msgstr "VideoLAN is opensource-software."

#: index.php:173
msgid "This means that if you have the skill and the desire to improve one of our products, your contributions are welcome"
msgstr "Dit betekent dat als u de kwaliteiten en het verlangen heeft om een van onze producten te verbeteren, uw bijdragen welkom zijn."

#: index.php:183
msgid "Spread the Word"
msgstr "Vertel het verder"

#: index.php:191
msgid "We feel that VideoLAN has the best video software available at the best price: free. If you agree please help spread the word about our software."
msgstr "We vinden dat VideoLAN de beste software beschikbaar heeft voor de beste prijs: gratis. Als u het daarmee akkoord bent, vertel dan verder over onze software."

#: index.php:211
msgid "News &amp; Updates"
msgstr "Nieuws &amp; updates"

#: index.php:214
msgid "More News"
msgstr "Meer nieuws"

#: index.php:218
msgid "Development Blogs"
msgstr "Ontwikkelingsblogs"

#: index.php:247
msgid "Social media"
msgstr "Sociale media"

#: vlc/index.php:3
msgid "Official page for VLC media player, the Open Source video framework!"
msgstr "Officiële pagina voor VLC media player, het open source video-framework"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "VLC voor"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Simpel, snel en krachtig"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Speelt alles"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Bestanden, schijven, webcams, apparaten en streams."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "Speelt de meeste codecs zonder noodzaak voor codec packs"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Draait op alle platforms"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Volledig gratis"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "geen spyware, geen reclame en geen user-tracking"

#: vlc/index.php:47
msgid "learn more"
msgstr "meer informatie"

#: vlc/index.php:66
msgid "Add"
msgstr "Toevoegen"

#: vlc/index.php:66
msgid "skins"
msgstr "skins"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Skins maken met"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "VLC skin editor"

#: vlc/index.php:72
msgid "Install"
msgstr "Installeren"

#: vlc/index.php:72
msgid "extensions"
msgstr "extensies"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Alle screenshots weergeven"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Officiële downloads van VLC media player"

#: vlc/index.php:146
msgid "Sources"
msgstr "Bronnen"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "U kunt ook rechtstreeks de"

#: vlc/index.php:148
msgid "source code"
msgstr "broncode verkrijgen"

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Kan mediaconversie en streaming doen."

#~ msgid "Discover all features"
#~ msgstr "Ontdek alle functies"

#~ msgid "A project and a"
#~ msgstr "Een project en een"

#~ msgid "composed of volunteers, developing and promoting free, open-source multimedia solutions."
#~ msgstr "opgebouwd uit vrijwilligers, die gratis, open source multimedia-oplossingen ontwikkelen en promoten."

#~ msgid "why?"
#~ msgstr "waarom?"

#~ msgid "Home"
#~ msgstr "Home"

#~ msgid "Support center"
#~ msgstr "Ondersteuningscentrum"

#~ msgid "Dev' Zone"
#~ msgstr "Dev' Zone"

#~ msgid "DONATE"
#~ msgstr "DONEREN"

#~ msgid "Other Systems and Versions"
#~ msgstr "Andere systemen en versies"

#~ msgid "Other OS"
#~ msgstr "Andere besturingssystemen"
